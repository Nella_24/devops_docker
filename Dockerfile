FROM ubuntu:20.04

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -yq --no-install-recommends \   
    # Install apache
    apache2 \
    # Install php 7.2
    libapache2-mod-php7.4 \
    # Install tools
    locales \
    php-yaml \
    && apt-get clean && rm -rf /var/lib/apt/lists/*

# Set locales
RUN locale-gen fr_FR.UTF-8
# Heure et date
RUN echo "Europe/Paris" > /etc/timezone
RUN cp /usr/share/zoneinfo/Europe/Paris /etc/localtime
RUN dpkg-reconfigure -f noninteractive tzdata
# Suppression du répertoire html
RUN rm -rf /var/www/html 

COPY src/ /var/www/

EXPOSE 80 443

WORKDIR /var/www/



CMD apachectl -D FOREGROUND 
