*** Settings ***
Resource         resources/common.resource
Suite Setup      Open browser to home page
Suite Teardown   Close browser

*** Test Cases ***
TC1
    Log    ${MESSAGE}
    Wait Until Page Contains    Hello
    
TC2 
    Should Be Equal    ${MESSAGE}    Hello, world!
